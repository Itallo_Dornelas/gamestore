import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: 50vh;
  background: var(--pink);
  background-image: linear-gradient(62deg, var(--pink) 0%, var(--purple) 100%);
  color: var(--gray);
  @media only screen and (min-width: 768px) {
    padding: 0 100px;
    min-height: 100vh;
  }
  h2 {
    font-size: 20px;
    font-weight: 400;
    margin-bottom: 50px;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-bottom: var(--vanilla) solid 4px;
    @media only screen and (min-width: 768px) {
      font-size: 30px;
    }
  }
  img {
    height: 250px;
    padding: 20px 0 20px 20px;
    transition: opacity 150ms ease-in-out;
  }
`;
