import { Container } from "./styles";
import Carousel from "react-multi-carousel";
import { Image } from "semantic-ui-react";
import "react-multi-carousel/lib/styles.css";

interface ContentProps {
  content: string;
  image: Array<string>;
}
const Content = ({ content, image }: ContentProps) => {
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  return (
    <Container>
      <h2>{content}</h2>
      <Carousel
        ssr
        responsive={responsive}
        showDots={true}
        infinite={true}
        autoPlay={true}
        autoPlaySpeed={3000}
      >
        {image.slice(0, 5).map((image, key) => {
          return <Image key={key} style={{ width: "100%" }} src={image} />;
        })}
      </Carousel>
    </Container>
  );
};
export default Content;
