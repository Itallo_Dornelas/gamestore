import styled from "styled-components";

export const Buttons = styled.button`
  background-color: var(--purple);
  color: var(--white);
  font-weight: bold;
  margin: 0 auto;
  border: 2px solid var(--gray);
  padding: 10px;
  max-width: 250px;
  border-radius: 12px;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  position: relative;
  &:hover {
    z-index: 2;
  }
  &:before {
    content: "";
    position: absolute;
    left: 0;
    width: 0;
    height: 100%;
    background: var(--gray);
    transition: 0.2s;
    z-index: -1;
    top: 0;
    border-radius: 10px;
  }
  :hover::before {
    width: 100%;
  }
`;
