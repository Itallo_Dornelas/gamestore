import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { products } from "../../mock/products";
import { useHistory } from "react-router-dom";
import { Buttons } from "../Button";
import { useValue } from "../../provider/filterValue";

export default function ControlledOpenSelect() {
  const [open, setOpen] = React.useState(false);
  const [filter, setFilter] = React.useState<string>("");
  const { setValue } = useValue();
  const history = useHistory();
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setFilter(event.target.value as string);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleFilter = (type: string) => {
    if (type === "de Z - A") {
      const filtred = products.sort((a, b) => {
        return a.name < b.name ? 1 : b.name < a.name ? -1 : 0;
      });
      setValue(filtred);
      history.push("/store");
    }
    if (type === "de A - Z") {
      const newProducts = products;
      const filtred = newProducts.sort((a, b) => a.name.localeCompare(b.name));
      setValue(filtred);
      history.push("/store");
    }
    if (type === "scoreMajor") {
      const filtred = products.sort((a, b) => b.score - a.score);
      setValue(filtred);
      history.push("/store");
    }
    if (type === "scoreMinor") {
      const filtred = products.sort((a, b) => a.score - b.score);
      setValue(filtred);
      history.push("/store");
    }
    if (type === "priceMajor") {
      const filtred = products.sort((a, b) => b.price - a.price);
      setValue(filtred);
      history.push("/store");
    }
    if (type === "priceMinor") {
      const filtred = products.sort((a, b) => a.price - b.price);
      setValue(filtred);
      history.push("/store");
    }
  };

  return (
    <div>
      <FormControl>
        <InputLabel id="demo-controlled-open-select-label">
          Ordenar por
        </InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={filter}
          onChange={handleChange}
        >
          <MenuItem value={"de A - Z"}>de A - Z</MenuItem>
          <MenuItem value={"de Z - A"}>de Z - A</MenuItem>
          <MenuItem value={"priceMajor"}>Maior Preço</MenuItem>
          <MenuItem value={"priceMinor"}>Menor Preço</MenuItem>
          <MenuItem value={"scoreMajor"}>Maior Popularidade</MenuItem>
          <MenuItem value={"scoreMinor"}>Menor Popularidade</MenuItem>
        </Select>
        <Buttons onClick={() => handleFilter(filter)}>pesquisar</Buttons>
      </FormControl>
    </div>
  );
}
