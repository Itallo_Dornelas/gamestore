import {
  AiFillGitlab,
  AiFillLinkedin,
  IoGameControllerOutline,
} from "react-icons/all";
import { Container, ContainerFooter } from "./styles";

const Footer = () => {
  return (
    <Container>
      <h3>
        <IoGameControllerOutline />
        Game.<span>Shop</span>
      </h3>

      <ContainerFooter>
        <nav>
          <a
            href="https://www.linkedin.com/in/itallo-dornelas/"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillLinkedin />
          </a>
          <a
            href="https://gitlab.com/Itallo_Dornelas"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillGitlab />
          </a>
        </nav>
      </ContainerFooter>
      <div>
        <p>&copy; 2021 itallo Dornelas.</p>
      </div>
    </Container>
  );
};
export default Footer;
