import styled from "styled-components";

export const Container = styled.div`
  background: var(--vanilla);
  background-image: linear-gradient(
    62deg,
    var(--orange) 0%,
    var(--vanilla) 100%
  );
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  padding-top: 10px;
  height: 200px;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  color: var(--gray);
  span {
    color: var(--pink);
  }
  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

export const ContainerFooter = styled.div`
  display: flex;
  justify-content: flex-start;
  svg {
    font-size: 40px;
    border-radius: 100%;
    color: var(--gray);
    :hover {
      color: var(--pink);
    }
  }
`;
