import { RiShoppingCartLine, IoGameControllerOutline } from "react-icons/all";
import { ContainerHeader, NavLink } from "./styles";
import MenuBurguer from "../Menu-Burguer";
import { useCarts } from "../../provider/cart";
import { Badge } from "@material-ui/core";

export default function PrimarySearchAppBar() {
  const { cart } = useCarts();

  return (
    <ContainerHeader>
      <div>
        <NavLink to="/">
          <h3>
            <IoGameControllerOutline />
            Game.<span>Shop</span>
          </h3>
        </NavLink>
        <div className="showMenu">
          <nav>
            <NavLink to="/store">
              <div>
                <h4>Games</h4>
              </div>
            </NavLink>
          </nav>
        </div>
        <nav>
          <NavLink to="/cart">
            <Badge badgeContent={cart.length} color="secondary">
              <RiShoppingCartLine size={20} />
            </Badge>
            <span> Carrinho </span>
          </NavLink>
        </nav>

        <div className="showMenuBurguer">
          <MenuBurguer />
        </div>
      </div>
    </ContainerHeader>
  );
}
