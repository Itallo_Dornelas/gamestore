import { Link } from "react-router-dom";
import styled from "styled-components";
export const ContainerHeader = styled.div`
  width: 100%;
  margin: 0 auto;
  overflow: hidden;
  position: relative;
  background-color: var(--purple);
  background-image: linear-gradient(62deg, var(--purple) 0%, var(--pink) 100%);
  box-shadow: 0.2rem 0.3rem 2px rgba(0, 0, 0, 0.6);

  div {
    display: flex;
    justify-content: space-between;
    text-align: center;
    align-items: center;
  }
  .showMenu {
    display: none;
  }
  @media only screen and (min-width: 768px) {
    padding: 30px 40px;
    .showMenu {
      display: flex;
      gap: 40px;
    }
    .showMenuBurguer {
      display: none;
    }
  }
`;
export const NavLink = styled(Link)`
  margin: 10px;
  font-size: 18px;
  color: var(--gray);
  :hover {
    color: var(--white);
  }
  h3 {
    color: var(--gray);
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    font-size: 1rem;
    @media only screen and (min-width: 576px) {
      font-size: 2rem;
    }
    span {
      color: var(--pink);
    }
  }
  h3:hover {
    color: var(--white);
  }
  > svg {
    transform: translateY(3.5px);
    margin-right: 10px;
  }
  span {
    margin-left: 5px;
  }
  div {
    border: var(--gray) solid 2px;

    h4 {
      font-size: 1.4rem;
      padding: 5px;
    }
  }
`;
