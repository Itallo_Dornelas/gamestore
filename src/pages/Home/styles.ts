import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 20px;
  min-height: 80vh;
  gap: 20px;
  text-align: center;
  @media only screen and (min-width: 768px) {
    flex-direction: row;
    align-items: center;
    padding: 0 100px;
  }
  div {
    width: 100%;
    @media only screen and (min-width: 768px) {
      width: 50%;
    }
  }

  h2 {
    font-size: 20px;
    font-weight: 400;
    margin-bottom: 50px;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    @media only screen and (min-width: 768px) {
      font-size: 30px;
    }
    span {
      color: var(--blue);
    }
  }
`;

export const Contain = styled.div`
  display: none;
  @media only screen and (min-width: 420px) {
    display: block;
  }
`;
