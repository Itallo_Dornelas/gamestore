import { Container, Contain } from "./styles";
import Lottie from "react-lottie";
import { Buttons } from "../../components/Button";
import { useHistory } from "react-router-dom";
import Footer from "../../components/Footer";
import animationHome from "../../components/Animations/Lottie/imageHome.json";
import Content from "../../components/Content";
import { images, imagesSugests, imagesPremius } from "../../mock/images";
const Home = () => {
  const history = useHistory();
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationHome,
  };

  return (
    <>
      <Container>
        <div>
          <h2>
            Os melhores <span>Jogos</span> da atualidade e os melhores preços!!
          </h2>
          <Buttons onClick={() => history.push("/store")}>
            Confira nossos produtos!
          </Buttons>
        </div>
        <Contain>
          <Lottie options={defaultOptions} height={400} />
        </Contain>
      </Container>
      <Content content={"Novidades"} image={images} />
      <Content content={"Sugestões para você"} image={imagesSugests} />
      <Content content={"Jogos premium"} image={imagesPremius} />
      <Footer />
    </>
  );
};
export default Home;
