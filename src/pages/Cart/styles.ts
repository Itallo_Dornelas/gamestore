import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column-reverse;
  width: 100%;
  align-items: center;

  @media (min-width: 768px) {
    flex-direction: row;
    align-items: flex-start;
    width: 100%;
  }

  div {
    background: var(--white);
    padding: 10px;
    border: 2px solid var(--vanilla);
    width: 80%;
    color: var(--gray);
    margin-right: 10px;

    div {
      display: flex;
      justify-content: space-between;
      flex-wrap: wrap;
      align-items: center;
      margin: 25px;
    }
  }
  .sectionPedidos {
    width: 100%;
    text-align: center;
    margin: 0 auto;
    @media (min-width: 768px) {
      width: 40%;
    }
    div {
      width: 100%;
      border: 2px solid var(--gray);
      background: var(--vanilla);
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
  }
`;

export const Image = styled.img`
  height: 220px;
  width: 90%;
  @media (min-width: 526px) {
    width: 50%;
  }
  @media (min-width: 768px) {
    width: 30%;
  }
`;

export const CardContainer = styled(Container)`
  padding: 0 0 8px;
  justify-content: space-between;
  margin-top: 15px;
`;

export const ContainerNull = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10%;
  align-items: center;
  h1 {
    color: var(--gray);
    margin-bottom: 10px;
  }
`;
