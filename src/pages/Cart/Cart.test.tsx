import { render, screen } from "@testing-library/react";
import renderer from "react-test-renderer";
import { CartContext } from "../../provider/cart";
import Cart from "./";
import { products } from "../../mock/products";

it("ele deve renderizar corretamente", () => {
  const setCart = jest.fn();
  const deleteCart = jest.fn();
  const tree = renderer
    .create(
      <CartContext.Provider value={{ cart: products, setCart, deleteCart }}>
        <Cart />
      </CartContext.Provider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("ele deve renderizar carrinho vazio", () => {
  const setCart = jest.fn();
  const deleteCart = jest.fn();
  render(
    <CartContext.Provider value={{ cart: [], setCart, deleteCart }}>
      <Cart />
    </CartContext.Provider>
  );
  const titleElement = screen.getByText("Carrinho vazio, vamos as compras?");
  expect(titleElement).toBeInTheDocument();
  const buttonElement = screen.getByText("Voltar aos Produtos");
  expect(buttonElement).toBeInTheDocument();
});
//--Test Button remove
// test("ele deve remover um produto do carrinho", () => {
//   let carts = products;
//   const setCart = jest.fn();
//   const deleteCart = jest.fn((cartToBeDeleted) => {
//     const newList = carts.filter((cart) => cart.name !== cartToBeDeleted.name);
//     carts = newList;
//   });
//   render(
//     <CartContext.Provider value={{ cart: carts, setCart, deleteCart }}>
//       <Cart />
//     </CartContext.Provider>
//   );
//   const buttonAdd = screen.getByText("Remover do carrinho");
//   expect(buttonAdd).toBeInTheDocument();
// });

// A cada produto adicionado, deve-se somar R$ 10,00 ao frete.
// test("ele deve somar R$ 10,00 ao frete", () => {
//   let newCart = products
//   const setCart = jest.fn();
//   const deleteCart = jest.fn();
//   render(
//     <CartContext.Provider value={{ cart: products, setCart, deleteCart }}>
//       <Cart />
//     </CartContext.Provider>
//   );
//   const titleElement = screen.getByText("Carrinho vazio, vamos as compras?");
//   expect(titleElement).toBeInTheDocument();
//   const buttonElement = screen.getByText("Voltar aos Produtos");
//   expect(buttonElement).toBeInTheDocument();
// });

//  O frete é grátis para compras acima de R$ 250,00.
