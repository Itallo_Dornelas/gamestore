import formatValue from "../../utils/index";
import { Container, Image, CardContainer, ContainerNull } from "./styles";
import { useHistory } from "react-router-dom";
import { useCarts } from "../../provider/cart";
import { Buttons } from "../../components/Button";
import React from "react";

const Cart = () => {
  const { cart, deleteCart } = useCarts();

  const history = useHistory();
  let frete: number = 0;
  const subtotal = cart
    .reduce((product, acc) => acc.price + product, 0)
    .toFixed(2);
  for (let i = 0; i < cart.length; i++) {
    if (parseInt(subtotal) < 250) {
      frete = frete + 10;
      if (parseInt(subtotal) > 250) {
        frete = 0;
      }
    }
  }

  const redirect = () => {
    history.push("/dashboard");
  };

  if (!cart.length) {
    return (
      <ContainerNull>
        <h1> Carrinho vazio, vamos as compras?</h1>
        <Buttons onClick={() => history.push("/store")}>
          Voltar aos Produtos
        </Buttons>
      </ContainerNull>
    );
  }

  return (
    <>
      <Container>
        <div>
          <div>
            <h3>{cart.length > 1 ? "Produtos" : "Produto"}</h3>
          </div>
          <section>
            {cart.map((product) => (
              <React.Fragment key={product.id}>
                <div>
                  <Image src={product.image} alt="Produto" />
                  <h4>{product.name}</h4>
                  {formatValue(product.price)}
                </div>
                <div>
                  <Buttons onClick={() => deleteCart(product)}>
                    Remover do Carrinho
                  </Buttons>
                </div>
              </React.Fragment>
            ))}
          </section>
        </div>
        <section className="sectionPedidos">
          <div>
            <h2>
              <strong>Resumo do pedido</strong>
            </h2>
            <CardContainer>
              <h4>{cart.length} Produtos</h4>
              <h4>{formatValue(parseInt(subtotal))}</h4>
              <h4>Compras acima de 250 reais Frete grátis!!!</h4>
              <h4>Frete {frete ? `${frete} reais` : " grátis!!"} </h4>
              <h4>{formatValue(parseInt(subtotal) + frete)}</h4>
            </CardContainer>
          </div>
          <div>
            <Buttons onClick={redirect}>Finalizar o pedido</Buttons>
          </div>
        </section>
      </Container>
    </>
  );
};

export default Cart;
