import { Container, ProductList, ContainerFilter } from "./styles";
import { useCarts } from "../../provider/cart";
import formatValue from "../../utils";
import { Buttons } from "../../components/Button";
import { useValue } from "../../provider/filterValue";
import {} from "react-icons/all";
import ControlledOpenSelect from "../../components/ControlledOpenSelect";

function Store() {
  const { setCart, cart } = useCarts();
  const { value } = useValue();

  return (
    <>
      <ContainerFilter>
        <ControlledOpenSelect />
      </ContainerFilter>
      <Container>
        <ProductList>
          {value.map((product) => (
            <li key={product.id}>
              <figure>
                <img src={product.image} alt={product.name} />
              </figure>
              <h2>{product.name}</h2>
              <div>
                <span>{formatValue(product.price)}</span>
                <Buttons onClick={() => setCart([...cart, product])}>
                  <span>Adicionar ao Carrinho</span>
                </Buttons>
              </div>
            </li>
          ))}
        </ProductList>
      </Container>
    </>
  );
}
export default Store;
