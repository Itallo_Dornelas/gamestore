import styled from "styled-components";
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10%;
  align-items: center;
  justify-content: center;
  h1 {
    color: var(--gray);
    margin-bottom: 10px;
  }
 
`;