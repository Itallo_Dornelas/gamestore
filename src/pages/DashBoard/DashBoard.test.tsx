import renderer from "react-test-renderer";

import DashBoard from ".";

it("ele deve renderizar corretamente", () => {
  const tree = renderer.create(<DashBoard />).toJSON();
  expect(tree).toMatchSnapshot();
});
