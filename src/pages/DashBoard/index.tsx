import { useHistory } from "react-router";
import { Buttons } from "../../components/Button";
import { useCarts } from "../../provider/cart";
import { Container } from "./styles";

const Dashboard = () => {
  const history = useHistory();
  const { setCart } = useCarts();
  return (
    <Container>
      <h1>Obrigado pela compra volte sempre!!</h1>
      <Buttons
        onClick={() => {
          localStorage.clear();
          history.push("/");
          const localProducts = localStorage.getItem("cart") || "[]";
          setCart(JSON.parse(localProducts));
        }}
      >
        Voltar as Compras
      </Buttons>
    </Container>
  );
};

export default Dashboard;
