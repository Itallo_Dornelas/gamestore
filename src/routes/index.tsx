import React from "react";
import Home from "../pages/Home";
import Store from "../pages/Store";
import Cart from "../pages/Cart";
import Dashboard from "../pages/DashBoard";

import { Switch, Route } from "react-router-dom";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/store" component={Store} />
      <Route path="/cart" component={Cart} />
      <Route path="/dashboard" component={Dashboard} />
    </Switch>
  );
};

export default Routes;
