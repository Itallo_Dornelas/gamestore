import { createContext, ReactNode, useContext, useState } from "react";
import { products } from "../../mock/products";

interface FilterValue {
  id?: number;
  name: string;
  price: number;
  image: string;
  score?: number;
}
interface ValueProviderData {
  value: FilterValue[];
  setValue: any;
}

interface ValueProviderProps {
  children: ReactNode;
}

export const ValueContext = createContext<ValueProviderData>(
  {} as ValueProviderData
);

export const ValueProvider = ({ children }: ValueProviderProps) => {
  const [value, setValue] = useState(products);

  return (
    <ValueContext.Provider value={{ value, setValue }}>
      {children}
    </ValueContext.Provider>
  );
};

export const useValue = () => useContext(ValueContext);
