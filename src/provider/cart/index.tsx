import { useEffect, SetStateAction, Dispatch } from "react";
import { createContext, ReactNode, useContext, useState } from "react";

interface Cart {
  id?: number;
  name: string;
  price: number;
  image: string;
  score?: number;
}
interface CartProviderData {
  cart: Cart[];
  setCart: Dispatch<SetStateAction<Cart[]>>;
  deleteCart: (cart: Cart) => void;
}

interface CartProviderProps {
  children: ReactNode;
}

export const CartContext = createContext<CartProviderData>(
  {} as CartProviderData
);

export const CartProvider = ({ children }: CartProviderProps) => {
  const [cart, setCart] = useState<Cart[]>([] as Cart[]);

  useEffect(() => {
    const localProducts = localStorage.getItem("cart") || "[]";
    setCart(JSON.parse(localProducts));
  }, []);

  useEffect(() => {
    if (cart && cart.length > 0) {
      localStorage.setItem("cart", JSON.stringify(cart));
    }
  }, [cart]);
  const deleteCart = (cartToBeDeleted: Cart) => {
    const newList = cart.filter((cart) => cart.name !== cartToBeDeleted.name);
    setCart(newList);
  };

  return (
    <CartContext.Provider value={{ cart, setCart, deleteCart }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCarts = () => useContext(CartContext);
