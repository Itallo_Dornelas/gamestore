import { ReactNode } from "react";
import { CartProvider } from "./cart";
import { ValueProvider } from "./filterValue";
interface ProvidersProps {
  children: ReactNode;
}
const Providers = ({ children }: ProvidersProps) => {
  return (
    <CartProvider>
      <ValueProvider>{children}</ValueProvider>
    </CartProvider>
  );
};

export default Providers;
