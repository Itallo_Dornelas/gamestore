export const images = [
  "https://blog.br.playstation.com/tachyon/sites/4/2020/04/48568579037_8ae3c56a21_k.jpg?resize=1088%2C612&crop_strategy=smart",
  "https://images-americanas.b2w.io/spacey/2020/01/02/Destaque_interna_games_2_mobile.png",
  "https://cdn.akamai.steamstatic.com/steam/apps/42700/capsule_616x353.jpg?t=1589829053",
  "https://www.outerspace.com.br/wp-content/uploads/2020/09/mario3dworldbowsersfury.jpg",
  "https://macmagazine.com.br/wp-content/uploads/2019/05/15-pokemon-rumble-rush.jpg",
];

export const imagesSugests = [
  "http://pm1.narvii.com/6744/207e4b81900d21f276cea11dc5b070d1f5671391v2_00.jpg",
  "https://olhardigital.com.br/uploads/acervo_imagens/2020/11/20201119163757.jpg",
  "https://1.bp.blogspot.com/-y_PTS_cea9M/XwdU0M6L_NI/AAAAAAAAWh4/-XlNt03FLhI5HS8tEGU796Sib1PrvLiLgCLcBGAsYHQ/s1600/IMG_20200709_142621%257E2.jpg",
  "https://super.abril.com.br/wp-content/uploads/2016/09/super_imgmelhores-games_12.jpg?quality=70&strip=all",
  "https://static-wp-tor15-prd.torcedores.com/wp-content/uploads/2019/12/news-article-images-star-wars-goh-splash.jpg",
];
export const imagesPremius = [
  "https://i.ytimg.com/vi/1K8MOBIyKLo/maxresdefault.jpg",
  "https://i0.wp.com/gamehall.com.br/wp-content/uploads/2019/05/GTA-4.jpg?resize=708%2C335&ssl=1",
  "http://curtojogos.com.br/wp-content/uploads/2017/11/Smite-Hand-Of-The-Gods-jogos-gratuito-pc-xbox-ps4.jpg",
  "https://cdn.ome.lt/Np7ietapcQpFKmj6KWZv0YkJp8I=/770x0/smart/uploads/conteudo/fotos/hearthstone_UTUh5Zp.jpg",
  "https://img.youtube.com/vi/Pvwdy38_vlo/mqdefault.jpg",
];
