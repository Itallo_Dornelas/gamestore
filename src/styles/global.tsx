import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
*{
    margin:0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
    &::-webkit-scrollbar {
    width: 10px;
    height: 5px;
  }

  ::-webkit-scrollbar-track {
    background: var(--white);
  }

  ::-webkit-scrollbar-thumb {
    background: var(--purple);
  }

  ::-webkit-scrollbar-thumb:hover {
    background: var(--gray);
  }
}

    :root{
            --white: #f5f5f5;
            --vanilla:#f1ede0;
            --orange: #feda93;
            --gray: #464353;
            --black: #2a2a2e;
            --blue: #52B0D9;
            --purple: #9371fd;
            --pink: #f4c4f7;
        }
    body{
        background: var(--vanilla);
        color: var(--gray)
        
    }   
    body,input, button{
        font-family: "Squada One", serif;
        font-size: 1rem;
    } 
    h1,h2,h3,h4,h5,h6{
        font-family: "Roboto Mono", monospace;
        font-weight: 700;
    }
    button, img{
        cursor: pointer;
    }
    a{
        text-decoration: none;
    }
`;
