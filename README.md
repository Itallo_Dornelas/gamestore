<div align=center>
<h1  align="center">
Game.Shop
</h1>
</div>
<div align="center">

![BADGE_WEB_REACT] ![BADGE_WEB_TYPESCRIPT]


<h2 text-align="center">
Game.Shop é um projeto utilizando a tecnologia React 
</h2>
</div>

## **:Game.Shop: OBJETIVO**

O projeto representa uma loja virtual para venda de jogos!

## **:computer: TECNOLOGIAS**


#### **Website** ([React][react])

    - **[React Router Dom][react_router_dom]**
    - **[React Icons][react_icons]**
    - **[Styled Components][styled-components]**
    - **[Material UI][@material-ui/core]** 
    - **[Lottie][react-lottie]**
    - **[Multi-Carousel] [react-multi-carousel]**
    
#### **Utilitários**

- Editor: **[Visual Studio Code][vscode]** &rarr;
- Commit Conventional: **[Commitlint][commitlint]**
- Fonte:**[Roboto-Mono][font_roboto-mono]**


## **:star2: DESENVOLVIDO POR**

<div align=center>

<table style="width:100%">
  <tr align=center>
    <th><strong>Itallo Dornelas</strong></th>
  </tr>
  <tr align=center>
    <td>
      <a href="https://gitlab.com/Itallo_Dornelas">
<img width="200" height="180" src="https://i.ibb.co/N3gr7GX/2020-05-07.jpg" alt="2020-05-07" border="0">
      </a>
    </td>
  </tr>
</table>

</div>



<!-- Badges -->
[BADGE_WEB_REACT]: https://img.shields.io/badge/web-react-blue
[BADGE_WEB_TYPESCRIPT]: https://img.shields.io/badge/react-TypeScript-blue






